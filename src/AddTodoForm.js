import React from 'react';

import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

const AddTodoForm = (props) => {
    return (
        <form onSubmit={props.addNewTodoSubmitted}>
            <input
                id="newTodo"
                name="newTodo"
                onChange={props.newTodoChanged}
                value={props.newTodo}
                placeholder="I need to..." />
            <DatePicker
                selected={props.startDate}
                onChange={props.handleDateChange}
                name="startDate"
                className="datePicker"
                placeholderText="Click to select a date"
            />
            <button type="submit" className="btnTodoAdd">Add Todo</button>
        </form>
    )
}

export default AddTodoForm;
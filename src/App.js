import React from 'react';
import './App.css';

import AddTodoForm from './AddTodoForm';
import TodoList from './TodoList';
import UndoItem from './UndoItem';

var placeholder = {};

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      message: "Todo list",
      undo: false,
      undoableListItem: {},
      newTodo: '',
      todos: JSON.parse(localStorage.getItem('todos')),
      startDate: undefined
    };
    this.handleDateChange = this.handleDateChange.bind(this);
    this.undoItem = this.undoItem.bind(this);
  }

  /** Gets the necessary details from the Date() object
  * @returns a string of the date
  * (without converting the Date object to a string, 
  * so the UTC based timezone issue doesn't happen) */ 
  formatDate(date) {
    // if the date has not been specified
    // returns an empty string
    if (!date) { return ""; }

    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];
  
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
  
    return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }

  addNewTodoSubmitted(event) {
    event.preventDefault();
    let title = this.state.newTodo;
    // returns from function if todo hasn't been specified
    if (title === "") {
      alert("You haven't entered a todo");
      return;
    }

    let dateTemp = this.state.startDate;
    let date = this.formatDate(dateTemp);
    let checked = false;

    if (localStorage.getItem('todos') == null) {
      let todos = [];
      todos.push({ title, date, checked });
      localStorage.setItem('todos', JSON.stringify(todos));
    } else {
      let todos = JSON.parse(localStorage.getItem('todos'));
      todos.push({ title, date, checked });
      localStorage.setItem('todos', JSON.stringify(todos));
    }
    this.setState({
      newTodo: '',
      todos: JSON.parse(localStorage.getItem('todos')),
      startDate: undefined
    });
  }

  newTodoChanged(event) {
    this.setState({
      newTodo: event.target.value
    });
  }

  handleDateChange(date) {
    this.setState({
      startDate: new Date(date)
    });
  }

  toggleChange(event, index) {
    let list = JSON.parse(localStorage.getItem('todos'));

    list[index].checked = event.target.checked;

    this.setState({
      todos: list
    });

    // update localStorage with the new checked status
    localStorage.setItem('todos', JSON.stringify(list));
  }

  deleteItem(event) {
    let index = event.target.getAttribute('data-key');
    let list = JSON.parse(localStorage.getItem('todos'));

    // store the removed item in undoableListItem 
    // so that it can be reinstated if needed
    this.setState({
      undoableListItem: list[index]
    })

    list.splice(index, 1);

    this.setState({
      todos: list,
      undo: true
    });

    localStorage.setItem('todos', JSON.stringify(list));
  }

  undoItem(event) {
    // get details from undoableListItem 
    let title = this.state.undoableListItem.title;
    let date = this.state.undoableListItem.date;
    let checked = this.state.undoableListItem.checked;

    let todos = JSON.parse(localStorage.getItem('todos'));

    todos.push({ title, date, checked });

    localStorage.setItem('todos', JSON.stringify(todos));

    this.setState({
      todos: JSON.parse(localStorage.getItem('todos')),
      undo: false
    });
  }

  orderMoveUp(event, index) {
    let list = JSON.parse(localStorage.getItem('todos'));

    if (!index == 0) {
      placeholder = list[index];
      list[index] = list[index - 1];
      list[index - 1] = placeholder;

      // update state
      this.setState({
        todos: list
      });

      // update localStorage
      localStorage.setItem('todos', JSON.stringify(list));
    } else {
      alert('This item cannot be moved up any further. Please choose another one!');
    }

    // remove focus after click
    document.activeElement.blur();
  }

  orderMoveDown(event, index) {
    let list = JSON.parse(localStorage.getItem('todos'));

    if (!(index == list.length - 1)) {
      placeholder = list[index];
      list[index] = list[index + 1];
      list[index + 1] = placeholder;

      // update state
      this.setState({
        todos: list
      });

      // update localStorage
      localStorage.setItem('todos', JSON.stringify(list));
    } else {
      alert("This item cannot be moved down any further. Please choose another one!");
    }

    // remove focus after click
    document.activeElement.blur();
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <h3>{this.state.message}</h3>

          <AddTodoForm
            addNewTodoSubmitted={this.addNewTodoSubmitted.bind(this)}
            newTodoChanged={this.newTodoChanged.bind(this)}
            newTodo={this.state.newTodo}
            startDate={this.state.startDate}
            handleDateChange={this.handleDateChange.bind(this)} />

          <TodoList
            todos={this.state.todos}
            toggleChange={this.toggleChange.bind(this)}
            deleteItem={this.deleteItem.bind(this)}
            orderMoveUp={this.orderMoveUp.bind(this)}
            orderMoveDown={this.orderMoveDown.bind(this)} />

          <UndoItem
            todos={this.state.todos}
            undo={this.state.undo}
            undoItem={this.undoItem.bind(this)} />

        </div>
      </div>
    );
  }
}

export default App;
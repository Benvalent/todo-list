import React from 'react';

const UndoItem = (props) => {
    return (
        <div
            className="undoItemSection"
            style={{
                transform: props.undo ? 'translate(0)' : 'translate(99999)',
                opacity: props.undo ? 1 : 0
            }}>
            Item deleted
          <button onClick={props.undo ? props.undoItem.bind(this) : null}>Undo</button>
        </div>
    )
}

export default UndoItem;
import React from 'react';
import TodoItem from './TodoItem';

const TodoList = (props) => {
    return (
        <ul>
            {props.todos == null ? '' : props.todos.map(function (todo, index) {
                return (
                    <TodoItem
                        key={index}
                        index={index}
                        todo={todo}
                        toggleChange={props.toggleChange}
                        deleteItem={props.deleteItem}
                        orderMoveUp={props.orderMoveUp}
                        orderMoveDown={props.orderMoveDown} />
                )
            }, this)}
        </ul>
    )
}

export default TodoList;
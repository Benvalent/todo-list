import React from 'react';

const TodoItem = (props) => {
    const { todo, index } = props;

    return (
        <li key={index}>
            <input
                type="checkbox"
                className="customCheckbox"
                onChange={(event) => props.toggleChange(event, index)}
                checked={todo.checked} />
            <span
                className="todoItemText"
                style={{
                    textDecoration: todo.checked ? 'line-through' : 'inherit',
                    color: todo.checked ? '#aaa' : 'inherit'
                }}>
                {todo.title}
            </span>
            <span className="todoItemDate">
                {todo.date}
            </span>
            <div className="orderButtons">
                <input
                    type="button"
                    value="&#10006;"
                    className="closeButton"
                    onClick={props.deleteItem.bind(this)}
                    data-key={index} />
                <div className="orderArrows">
                    <button onClick={(event) => props.orderMoveUp(event, index)}>&#9650;</button>
                    <button onClick={(event) => props.orderMoveDown(event, index)}>&#9660;</button>
                </div>
            </div>
        </li>
    )
}

export default TodoItem;